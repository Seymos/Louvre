/**
 * Variables
 */
var panel_container = $(".panel-container");
var section_center = $("section#center");
var collapse_item = $(".panel-title > a");
var collapse = $("a[data-toggle='collapse']");
var navigation = $(".panel-body nav");
var nav2 = $("nav.menu");
var btn2 = $(".panel-menu button");
var ico2 = $(".panel-menu button i");
var panel_body = $(".panel-body");
var ico_rsp = $("#btn_responsive i");
var first_collapse = $("a[data-toggle='collapse']:first-child");
var btn_panier = $("#btn_panier");
var panier = $(".panier");