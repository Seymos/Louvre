$(function () {
    console.log('app.js loaded');
    setFullHeight(section_center);
    if (window.innerHeight < 550) {
        panel_container.css('height','auto');
    } else {
        setFullHeight(panel_container);
    }

    navigationResponsive();
    secondNavigation();
    showPanier();
});

