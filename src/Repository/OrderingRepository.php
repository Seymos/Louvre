<?php

namespace App\Repository;

use App\Entity\Ordering;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class OrderingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ordering::class);
    }

	public function findToDay()
	{
        $today = new \DateTime('now');
		return $this->createQueryBuilder('t')
		            ->where('t.day = :day')->setParameter('day', $today->format('Y/m/d'))
		            ->getQuery()
		            ->getResult()
			;
	}

	public function findOneByCodeUnique($code)
	{
		return $this->createQueryBuilder('o')
			->select('o')
			->where('o.code = :code')
			->setParameter('code',$code)
			->setMaxResults(1)
			->getQuery()->getResult();
	}

	public function findOrderingTicket($id)
	{
		return $this->createQueryBuilder('o')
		            ->select('o')
		            ->join('ordering_tickets','t')
		            ->where('t.ordering_id = :id')
		            ->setParameter('id', $id)
		            ->getQuery()
		            ->getResult()
			;
	}

	public function example()
	{
		return $this->createQueryBuilder('o')
		            ->where('o.something = :value')->setParameter('value', $value)
		            ->orderBy('o.id', 'ASC')
		            ->setMaxResults(10)
		            ->getQuery()
		            ->getResult()
			;
	}
}
