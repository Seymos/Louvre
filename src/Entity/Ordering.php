<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Validator\Constraints\CheckDate;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderingRepository")
 * @UniqueEntity("code")
 * @ORM\HasLifecycleCallbacks()
 */
class Ordering
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="date", name="day")
	 * @Assert\Date()
     * @CheckDate()
	 */
    private $day;

    /**
     * @ORM\Column(type="date", name="date_order")
     */
    private $date_order;

	/**
	 * @ORM\Column(type="string", name="type")
	 * @Assert\NotBlank()
	 */
    private $type;

	/**
	 * @ORM\Column(type="string", name="email")
	 * @Assert\NotBlank(message="Ce champ est obligatoire")
	 * @Assert\Email(
	 *     message = "Merci de saisir un email valide.",
	 *     checkMX = true
	 *     )
	 */
    private $email;

	/**
	 * @ORM\Column(type="integer", name="number")
     * @Assert\NotBlank(message="Ce champ est obligatoire")
	 * @Assert\Range(
	 *     min = 1,
	 *     max = 1000,
	 *     minMessage = "Vous devez indiquer au moins un participant pour valider la commande.",
	 *     maxMessage = "Vous dépassez la capacité du musée, merci de saisir moins de participants."
	 * )
	 */
    private $number;

	/**
	 * @ORM\Column(name="code", type="string", length=255)
	 */
    private $code;

    /**
     * @ORM\Column(type="boolean", name="paied")
     */
    private $paied;

    /**
     * @ORM\Column(type="boolean", name="email_sent")
     */
    private $email_sent;

	/**
	 * One Ordering has Many Tickets.
	 * @ORM\Column(name="tickets", type="array")
	 * @ORM\OneToMany(targetEntity="Ticket", mappedBy="ordering", cascade="persist")
	 * @ORM\JoinColumn(nullable=false)
	 */
    private $tickets;

	/**
	 * Ordering constructor.
	 */
	public function __construct() {
		$this->date_order = new \DateTime();
		$this->tickets = new ArrayCollection();
        $this->code = $this->getDateOrder()->format('d').'-'.substr(uniqid(true),0,10);
        $this->paied = false;
        $this->email_sent = false;
	}

	public function hydrate(string $type, string $email, int $number)
    {
        $this->day = new \DateTime('now');
        $this->date_order = new \DateTime('now');
        $this->type = $type;
        $this->email = $email;
        $this->number = $number;
        $this->code = $this->getDateOrder()->format('d').'-'.substr(uniqid(true),0,10);
        $this->paied = false;
        $this->email_sent = false;
    }

	/**
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param integer $id
	 */
	public function setId( $id ) {
		$this->id = $id;
	}

    /**
     * @return \DateTime
     */
	public function getDay() {
		return $this->day;
	}

    /**
     * @param $day
     */
	public function setDay( $day ) {
		$this->day = $day;
	}

    /**
     * @return \DateTime
     */
    public function getDateOrder()
    {
        return $this->date_order;
    }

    /**
     * @param $date_order
     */
    public function setDateOrder($date_order)
    {
        $this->date_order = $date_order;
    }

	/**
	 * @return integer
	 */
	public function getNumber() {
		return $this->number;
	}

	/**
	 * @param integer $number
	 */
	public function setNumber( $number ) {
		$this->number = $number;
	}

	/**
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType( $type ) {
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail( $email ) {
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getCode() {
		return $this->code;
	}

	public function setCode() {
		$this->code = $this->getDay()->format('d').'-'.substr(uniqid(true),0,10);
	}

	public function addTicket(Ticket $ticket) {
		$this->tickets[] = $ticket;
	}

    /**
     * @return boolean
     */
    public function getPaied()
    {
        return $this->paied;
    }

    /**
     * @param boolean $paied
     */
    public function setPaied($paied)
    {
        $this->paied = $paied;
    }

    /**
     * @return boolean
     */
    public function getEmailSent()
    {
        return $this->email_sent;
    }

    /**
     * @param boolean $email_sent
     */
    public function setEmailSent($email_sent): void
    {
        $this->email_sent = $email_sent;
    }

	/**
	 * @return Collection|Ticket[]
	 */
	public function getTickets() {
		return $this->tickets;
	}

	/**
	 * @param Collection|Ticket $tickets
	 */
	public function setTickets( $tickets ) {
		$this->tickets = $tickets;
	}
}
