<?php

namespace App\Form;

use App\Entity\Ordering;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderingType extends AbstractType
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
            	'day',
	            DateType::class,
	            array(
	            	'label' => "Jour de la visite",
		            'attr' => array(
			            'class' => "form-control datepicker"
		            ),
		            'widget' => 'single_text',
		            'html5' => false,
		            'format' => "dd/MM/yyyy",
		            'model_timezone' => "Europe/Paris",
	            )
            )
	        ->add(
	        	'type',
		        ChoiceType::class,
		        array(
		        	'label' => "Type de billet",
		        	'choices' => array(
		        		'Billet Journée' => "J",
				        'Billet Demi-Journée' => "D",
			        ),
		        	'attr' => array(
		        		'class' => "form-control"
			        )
		        )
	        )
            ->add(
            	'email',
	            EmailType::class,
	            array(
	            	'label' => "Votre email pour l'envoi de vos billets",
		            'attr' => array(
			            'class' => "form-control",
			            'placeholder' => "Votre e-mail"
		            )
	            )
            )
	        ->add(
	        	'number',
		        IntegerType::class,
		        array(
		        	'label' => "Nombre de participants",
		        	'attr' => array(
		        		'class' => "form-control",
				        'placeholder' => "Nombre de participants"
			        )
		        )
	        )
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $this->session->set('pre_submit',$event->getData());
            })
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ordering::class
        ]);
    }
}
