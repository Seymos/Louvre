<?php

namespace App\Form;

use App\Entity\Ticket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
            	'lastname',
	            TextType::class,
	            array(
	            	'label' => "Votre NOM de famille",
		            'attr' => array(
		            	'class' => "form-control"
		            )
	            )
            )
            ->add(
            	'firstname',
	            TextType::class,
	            array(
	            	'label' => "Votre Prénom",
		            'attr' => array(
		            	'class' => "form-control"
		            )
	            )
            )
            ->add(
            	'country',
	            CountryType::class,
	            array(
	            	'label' => "Sélectionnez votre pays",
		            'attr' => array(
		            	'class' => "form-control"
		            ),
	            )
            )
            ->add(
            	'birthdate',
	            BirthdayType::class,
	            array(
	            	'label' => "Saisissez votre date de naissance",
		            'attr' => array(
		            	'class' => ""
		            )
	            )
            )
	        ->add(
	        	'promotion',
		        CheckboxType::class,
		        array(
		        	'required' => false,
		        	'label' => "Bénéficiez-vous du tarif réduit ?"
		        )
	        )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            'data_class' => Ticket::class
        ]);
    }
}
