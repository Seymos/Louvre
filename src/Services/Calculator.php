<?php

namespace App\Services;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Calculator {

	public function total($number, SessionInterface $session) {
		for ($i = 0; $i < $number; $i++) {
			// récupération tarif courant
			$current_price = $session->get('ticket'.$i)->getPricing();
			// addition total actuel + tarif courant
			$total = $session->get('total') + $current_price;
			$session->set('total',$total);
		}
		return $total;
	}

	public function getNumberOfTickets($orders, SessionInterface $session) {
		$session->set('total_tickets',0);
		$total = 0;
		foreach ($orders as $order) {
			$count = $order->getNumber();
			$total = $session->get('total_tickets') + $count;
		}
		return $total;
	}
}
