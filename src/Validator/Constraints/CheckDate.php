<?php

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CheckDate extends Constraint {

	public function validatedBy() {
		return get_class($this)."Validator";
	}
}
