<?php
namespace App\Tests\Entity;

use App\Entity\Ordering;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class OrderingTest extends TestCase
{
    private $ordering;

    public function setup()
    {
        $this->ordering = new Ordering();
        $this->ordering->setNumber(1);
        $this->ordering->setType("D");
        $this->ordering->setEmail("contact@louisthomas.fr");
        $date = new \DateTime('09/05/2018 10:30');
        $day_visit = $date->format('d/m/Y H:i');
        $this->ordering->setDay($day_visit);
    }

    public function testObjectIsInstanceOfOrdering()
    {
        $this->assertInstanceOf(Ordering::class, $this->ordering);
    }

    public function testNumberOfTicketIsNotNull()
    {
        $this->assertEquals(1, $this->ordering->getNumber());
        $this->assertSame(1, $this->ordering->getNumber());
        $this->assertLessThanOrEqual(1, $this->ordering->getNumber());
        $this->assertGreaterThan(0, $this->ordering->getNumber());
    }

    public function testTicketTypeIsAHalfDay()
    {
        $this->assertEquals("D", $this->ordering->getType());
    }

    public function testEmailIsValid()
    {
        $this->assertRegExp("#^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$#", $this->ordering->getEmail(), "L'adresse email est invalide.");
    }

    public function testQuantityIsNotNull()
    {
        $this->ordering->setNumber(3);
        $this->assertNotNull($this->ordering->getNumber(), "Le nombre de participants est invalide.");
    }
}
