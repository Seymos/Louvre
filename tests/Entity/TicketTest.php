<?php
namespace App\Tests\Entity;

use App\Entity\Ticket;
use PHPUnit\Framework\TestCase;

class TicketTest extends TestCase
{
    private $ticket;

    public function setup()
    {
        $this->ticket = new Ticket();
        $this->ticket->setPricing(20.0);
    }

    public function testTicketIsInstanceOfTicket()
    {
        $this->assertInstanceOf(Ticket::class, $this->ticket, "Not an instance of Ticket::class");
    }

    public function testPricingIsNotNull()
    {
        $this->assertNotNull($this->ticket->getPricing());
    }
}
