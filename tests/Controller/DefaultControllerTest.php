<?php
/**
 * Created by PhpStorm.
 * User: seymos
 * Date: 13/03/18
 * Time: 13:54
 */

namespace App\Tests\Controller;

use App\Entity\Ordering;
use App\Entity\Ticket;
use App\Form\TicketType;
use App\Tests\Entity\OrderingTest;
use App\Tests\Entity\TicketTest;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Form;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Tests\Extension\Core\Type\CollectionTypeTest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage;

class DefaultControllerTest extends WebTestCase
{
    /**
     * @return array
     */
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $form = $crawler->selectButton('Suivant')->form();
        $form['ordering[day]'] = '02/06/2020';
        $form['ordering[number]'] = 1;
        $form['ordering[type]'] = "J";
        $form['ordering[email]'] = "louisthomas76750@gmail.com";
        $client->submit($form);

        $this->assertInstanceOf(Form::class, $form);
        $values = $form->getValues();
        $this->assertArrayHasKey('ordering[day]', $values);
        $this->assertArrayHasKey('ordering[number]', $values);
        $this->assertArrayHasKey('ordering[type]', $values);
        $this->assertArrayHasKey('ordering[email]', $values);
        $this->assertArrayHasKey('ordering[_token]', $values);

        $this->assertGreaterThanOrEqual('02/06/2020', $values['ordering[day]']);
        $this->assertGreaterThanOrEqual('1', $values['ordering[number]']);
        $this->assertNotNull($values['ordering[number]']);
        $this->assertEquals("J", $values['ordering[type]']);
        $this->assertEquals("louisthomas76750@gmail.com", $values['ordering[email]']);

        $date = date('25/04/2018');
        $ordering = new Ordering();
        $ordering->setDay($values['ordering[day]'] = "25/04/2018");
        $ordering->setEmail($values['ordering[email]'] = "contact@louisthomas.fr");
        $ordering->setDateOrder($date);
        $ordering->setNumber($values['ordering[number]'] = 1);
        $ordering->setType($values['ordering[type]'] = "J");
        $ordering->setTickets(new ArrayCollection());

        $this->assertInstanceOf(Ordering::class, $ordering);
        $this->assertInstanceOf(ArrayCollection::class, $ordering->getTickets());
        $this->assertGreaterThanOrEqual($date, $ordering->getDay());
        $this->assertEquals("contact@louisthomas.fr", $ordering->getEmail());
        $this->assertEquals($date, $ordering->getDateOrder());
        $this->assertGreaterThanOrEqual(1, $ordering->getNumber());
        $this->assertEquals("J", $ordering->getType());
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        return [
            [$ordering]
        ];
    }

    /**
     * @dataProvider testIndex
     * @param $ordering
     */
    public function testInformation($ordering)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/informations');
        $client->followRedirect();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $nb_visitors = $ordering->getNumber();
        $tickets = array();
        for ($i = 1; $i <= $nb_visitors; $i++) {
            $ticket = new Ticket();
            $promotion = ($i%2 === 0) ? true : false;
            $ticket->setPromotion($form['ticket[promotion]'] = true);
            $ticket->setBirthdate($form['ticket[birthdate]'] = date('20/02/1990'));
            $ticket->setFirstname($form['ticket[firstname]'] = "Louis");
            $ticket->setLastname($form['ticket[lastname]'] = "THOMAS");
            $ticket->setCountry($form['ticket[country]'] = "France");
            $ticket->setPricingWith();
            array_push($tickets, $ticket);
        }
        $form = $crawler->selectButton("Valider")->form();
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertEquals($nb_visitors, sizeof($tickets));
    }

    public function testPayment()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/payment');
        $client->followRedirect();
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
